package ba.etf.rma21.projekat.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class KvizViewModel : ViewModel() {

    private val quizRepo = KvizRepository.Companion
    val kvizovi: MutableLiveData<List<Kviz>> by lazy {
        MutableLiveData<List<Kviz>>()
    }
    val pitanja: MutableLiveData<List<Pitanje>?> by lazy {
        MutableLiveData<List<Pitanje>?>()
    }
    val pokusaj: MutableLiveData<KvizTaken?> by lazy {
        MutableLiveData<KvizTaken?>()
    }
    val user: MutableLiveData<Account> by lazy {
        MutableLiveData<Account>()
    }

    fun getAll() {
        viewModelScope.launch(Dispatchers.IO) {
            kvizovi.postValue(quizRepo.getAll())
        }
    }

    fun getUpisani() {
        viewModelScope.launch(Dispatchers.IO) {
            DBRepository.updateNow()
            kvizovi.postValue(quizRepo.getUpisaniDB())
        }
    }

    fun getDone() {
        viewModelScope.launch(Dispatchers.IO) {
            DBRepository.checkUpdate()
            kvizovi.postValue(quizRepo.getDone())
        }
    }

    fun getFuture() {
        viewModelScope.launch(Dispatchers.IO) {
            DBRepository.checkUpdate()
            kvizovi.postValue(quizRepo.getFuture())
        }
    }

    fun getNotTaken() {
        viewModelScope.launch(Dispatchers.IO) {
            DBRepository.checkUpdate()
            kvizovi.postValue(quizRepo.getNotTaken())
        }
    }

    fun getPitanja(kviz: Kviz) {
        viewModelScope.launch(Dispatchers.IO) {
            DBRepository.checkUpdate()
            pitanja.postValue(PitanjeKvizRepository.getPitanjaDB(kviz.id))
        }
    }

    fun zapocniKviz(idKviza: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            var kvizTaken = TakeKvizRepository.zapocniKviz(idKviza)
            if (kvizTaken == null)
                kvizTaken = TakeKvizRepository.dajPokusaj(idKviza)
            pokusaj.postValue(kvizTaken)
        }
    }

    fun updateBodove(kid: Int, bodici: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            KvizRepository.updateBodici(kid, bodici)
        }
    }

    fun postaviOdgovorKviz(ktid: Int, pitanje: Int, answer: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val bodici = OdgovorRepository.postaviOdgovorKviz(ktid, pitanje, answer)
            pokusaj.value?.apply { osvojeniBodovi = bodici }
        }
    }

    fun postaviHash(hash: String) {
        viewModelScope.launch(Dispatchers.IO) {
            if(AccountRepository.postaviHash(hash))
                user.postValue(AccountRepository.getUser())
        }
    }

    fun predajOdgovore(kvizId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            if(OdgovorRepository.predajOdgovore(kvizId)) {
                KvizRepository.zavrsiKviz(kvizId)
            }
        }
    }
}