package ba.etf.rma21.projekat.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.DBRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UpisViewModel : ViewModel() {
    private val kvizRepo = KvizRepository.Companion
    private val predmetiGrupeRepo = PredmetIGrupaRepository.Companion

    val predmeti: MutableLiveData<List<Predmet>> by lazy {
        MutableLiveData<List<Predmet>>()
    }
    val grupe: MutableLiveData<List<Grupa>> by lazy {
        MutableLiveData<List<Grupa>>()
    }

    fun getGrupe(nazivPredmeta: String?) {
        if (nazivPredmeta == null) grupe.postValue(emptyList())
        else{
            viewModelScope.launch(Dispatchers.IO) {
                val idPredmeta = predmeti.value?.first { predmet -> predmet.naziv == nazivPredmeta }?.id
                if (idPredmeta != null)
                    grupe.postValue(predmetiGrupeRepo.getGrupeZaPredmet(idPredmeta))
                else grupe.postValue(emptyList())
            }
        }
    }

    fun upisiUGrupu(idGrupe: Int) {
        viewModelScope.launch {
            val upisan = PredmetIGrupaRepository.upisiUGrupu(idGrupe)
            if (upisan) DBRepository.checkUpdate()
        }
    }

    fun dajPredmete(godina: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val predmeti = predmetiGrupeRepo.getPredmeti()
                .filter { predmet -> predmet.godina == godina }
            val upisaniPredmeti = predmetiGrupeRepo.getUpisaneGrupe().map { grupa -> grupa.nazivPredmeta }
            this@UpisViewModel.predmeti.postValue(predmeti.filter { predmet -> !upisaniPredmeti.contains(predmet.naziv) })
        }
    }
}