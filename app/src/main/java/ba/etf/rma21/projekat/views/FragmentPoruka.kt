package ba.etf.rma21.projekat.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa

class FragmentPoruka : Fragment() {
    private var grupa : Grupa? = null
    private var kviz : String? = null
    private var procenat : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        arguments?.let {
            grupa = it.getParcelable("GRUPA")
            kviz = it.getString("KVIZ")
            procenat = it.getInt("PROCENAT")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_poruka, container, false)
        val textView = view.findViewById<TextView>(R.id.tvPoruka)
        when (container?.id) {
            R.id.fragment_container -> {
                textView.text = "Uspješno ste upisani u grupu ${grupa?.naziv} predmeta ${grupa?.nazivPredmeta}!"
            }
            R.id.framePitanje -> {
                textView.text = "Završili ste kviz ${kviz} sa tačnosti $procenat%"
            }
        }

        return view
    }

    companion object {
        fun newInstance() = FragmentPoruka()
    }
}