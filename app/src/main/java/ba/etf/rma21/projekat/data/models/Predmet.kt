package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Predmet(@PrimaryKey val id: Int,
                   @ColumnInfo(name = "naziv") val naziv: String,
                   @ColumnInfo(name = "godina") val godina: Int) {
    override fun toString(): String {
        return naziv
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Predmet

        if (id != other.id) return false
        if (naziv != other.naziv) return false
        if (godina != other.godina) return false

        return true
    }
}