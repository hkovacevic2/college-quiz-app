package ba.etf.rma21.projekat.interfaces

interface LoadPitanjeListener {
    fun findData() : Array<Triple<Int, Int, Int>?>?
    fun getPercentage(): Double
    fun saveData(boolean: Boolean, data: Array<Triple<Int, Int, Int>?>)
}
