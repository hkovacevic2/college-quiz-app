package ba.etf.rma21.projekat.interfaces

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje

interface PopupListener {
    fun startKvisko(kviz: Kviz, pitanja: List<Pitanje>)
}