package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Odgovor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class OdgovorRepository {
    companion object {
        private lateinit var context : Context
        fun setContext(_context : Context){
            context=_context
        }

        suspend fun getOdgovoriDB(kid: Int) : List<Odgovor> {
            val db = AppDatabase.getInstance(context)
            return db.odgovorDao().getOdgovoriZaKviz(kid)
        }

        suspend fun getOdgovori(idPokusaja: Int) : List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                try {
                    val odgovori = arrayListOf<Odgovor>()
                    val url = ApiConfig.baseURL+"/student/"+AccountRepository.acHash+"/kviztaken/$idPokusaja/odgovori"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val jsonOdgovori = JSONArray(result)
                        for (i in 0 until jsonOdgovori.length()) {
                            val jsonOdg = jsonOdgovori.getJSONObject(i)
                            odgovori.add(Odgovor(jsonOdg.getInt("PitanjeId"),jsonOdg.getInt("odgovoreno"), jsonOdg.getInt("KvizId")))
                        }
                        return@withContext odgovori
                    }
                } catch (e: MalformedURLException) {
                    return@withContext null
                } catch (e: IOException) {
                    return@withContext null
                } catch (e: JSONException) {
                    return@withContext null
                }
            }
        }

        suspend fun getOdgovoriKviz(idKviza: Int) : List<Odgovor> {
            return withContext(Dispatchers.IO) {
                var ktid : Int = -1
                try {
                    val pokusaji = TakeKvizRepository.getPokusajiJSON()
                    for (i in 0 until pokusaji.length()) {
                        val pokusaj = pokusaji.getJSONObject(i)
                        if (pokusaj.getInt("KvizId") == idKviza) {
                            ktid = pokusaj.getInt("id")
                            break
                        }
                    }
                    return@withContext getOdgovori(ktid) ?: emptyList()
                } catch (e: MalformedURLException) {
                    return@withContext emptyList<Odgovor>()
                } catch (e: IOException) {
                    return@withContext emptyList<Odgovor>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Odgovor>()
                }
            }
        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int {
            return withContext(Dispatchers.IO) {
                try {
                    val db = AppDatabase.getInstance(context)
                    DBRepository.setContext(context)
                    DBRepository.checkUpdate()
                    TakeKvizRepository.setContext(context)
                    val pokusaj = TakeKvizRepository.getPokusaj(idKvizTaken)
                    pokusaj?.run {
                        db.odgovorDao().insert(Odgovor(idPitanje, odgovor, KvizId))
                        val pitanja = db.pitanjeDao().getPitanjaZaKviz(KvizId)
                        val odgovori = pitanja.map { pitanje -> db.odgovorDao().getOdgovor(KvizId, pitanje.id) }
                        var tacni = 0
                        odgovori.filterNotNull().forEach { odgovor ->
                            val pitanje = pitanja.first { it.id == odgovor.id }
                            if (pitanje.tacan == odgovor.odgovoreno) tacni++
                        }
                        return@withContext (tacni.toDouble()/pitanja.size * 100).toInt()
                    }
                    return@withContext -1
                } catch (e: MalformedURLException) {
                    return@withContext -1
                } catch (e: IOException) {
                    return@withContext -1
                } catch (e: JSONException) {
                    return@withContext -1
                }

            }
        }

        suspend fun predajOdgovore(kid: Int) : Boolean {
            return withContext(Dispatchers.IO) {
                try {
                    val db = AppDatabase.getInstance(context)
                    val odgovori = db.odgovorDao().getOdgovoriZaKviz(kid)
                    val pokusaj = TakeKvizRepository.dajPokusaj(kid)
                    for (odgovor in odgovori) {
                        pokusaj?.run {
                            val url = ApiConfig.baseURL+"/student/"+AccountRepository.acHash+"/kviztaken/$id/odgovor"
                            val URL = URL(url)
                            val konekcija = URL.openConnection() as HttpURLConnection
                            konekcija.run {
                                requestMethod = "POST"
                                this.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                                this.setRequestProperty("Accept", "application/json")
                                doOutput = true
                                val json = JSONObject()
                                json.put("odgovor", odgovor.odgovoreno).put("pitanje", odgovor.id).put("bodovi", osvojeniBodovi)
                                this.outputStream.write(json.toString().toByteArray(charset("UTF-8")))
                                val result = this.inputStream.bufferedReader().use { it.readText() }
                                val rez = JSONObject(result)
                                if (rez.has("message")) return@withContext false
                            }
                        }
                    }
                    return@withContext true
                } catch (e: MalformedURLException) {
                    return@withContext false
                } catch (e: IOException) {
                    return@withContext false
                } catch (e: JSONException) {
                    return@withContext false
                }
            }
        }
    }
}