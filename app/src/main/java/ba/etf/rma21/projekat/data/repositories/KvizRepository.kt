package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Kviz
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class KvizRepository {
    companion object {
        private val stanja = mutableMapOf<Int, Boolean>()
        private lateinit var context : Context
        fun setContext(_context:Context){
            context=_context
        }

        private fun toDate(string: String?): Date? {
            if(string.equals(null)) return null
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            return dateFormat.parse(string)
        }

        private fun getDate(dan: Int, mjesec: Int, godina: Int): Date {
            val cal = Calendar.getInstance()
            cal.clear()
            cal.set(godina, mjesec - 1, dan)
            return cal.time
        }

        private fun getColor(kviz: Kviz): Int {
            val atm = Calendar.getInstance().time
            if (atm.before(toDate(kviz.datumKraj))) {
                if (atm.before(toDate(kviz.datumPocetka)))
                    return R.drawable.zuta
                return R.drawable.zelena
            } else {
                if (kviz.datumRada == null)
                    return R.drawable.crvena
                return R.drawable.plava
            }
        }

        private suspend fun makeKvisko(kvizJSON : JSONObject) : Kviz? {
            if (kvizJSON.has("message")) return null
            val id = kvizJSON.getInt("id")
            val dateStartString = kvizJSON.getString("datumPocetak")
            val dateStart =
                if (dateStartString.equals("null")) null
                else dateStartString + "T09:00:00"
            val dateEndString = kvizJSON.get("datumKraj")
            val dateEnd =
                if (dateEndString.equals(null)) "2025-07-10T09:00:00"
                else dateEndString.toString() + "T09:00:00"
            val naziv = kvizJSON.getString("naziv")
            val grupe = PredmetIGrupaRepository.getGrupeZaKviz(id)
            var nazivGrupe = ""
            for (i in grupe.indices) {
                if(i + 1 != grupe.size) nazivGrupe += "${grupe[i].naziv},"
                else nazivGrupe += grupe[i].naziv
            }
            val nazivPredmeta = grupe.first().nazivPredmeta
            val trajanje = kvizJSON.getInt("trajanje")
            return Kviz(id, naziv, nazivPredmeta, dateStart, dateEnd, null, trajanje, nazivGrupe, 0, false)
        }

        suspend fun getAll() : List<Kviz> {
            return withContext(Dispatchers.IO) {
                try {
                    val kvizovi = arrayListOf<Kviz>()
                    val url = ApiConfig.baseURL+"/kviz"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val json = JSONArray(result)

                        for (i in 0 until json.length()) {
                            makeKvisko(json.getJSONObject(i))?.let { kvizovi.add(it) }
                        }
                    }
                    return@withContext kvizovi
                } catch (e: MalformedURLException) {
                    return@withContext emptyList<Kviz>()
                } catch (e: IOException) {
                    return@withContext emptyList<Kviz>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Kviz>()
                }
            }
        }

        suspend fun getById(id: Int) : Kviz? {
            return withContext(Dispatchers.IO) {
                try {
                    val url = ApiConfig.baseURL+"/kviz/$id"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        return@withContext makeKvisko(JSONObject(result))
                    }
                }catch (e: MalformedURLException) {
                    return@withContext null
                } catch (e: IOException) {
                    return@withContext null
                } catch (e: JSONException) {
                    return@withContext null
                }
            }
        }

        suspend fun updateBodici(kid: Int, bodici: Int) {
            val db = AppDatabase.getInstance(context)
            db.kvizDao().updateBodove(kid, bodici)
        }

        suspend fun getUpisaniDB() : List<Kviz> {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                return@withContext db.kvizDao().getMyAll()
            }
        }

        suspend fun getUpisani() : List<Kviz> {
            return withContext(Dispatchers.IO) {
                val mapa = PredmetIGrupaRepository.getUpisaneGrupe().map { grupa -> Pair(grupa.id,grupa.nazivPredmeta) }
                val upisani = arrayListOf<Kviz>()
                for (par in mapa) upisani.addAll(getKvizoviZaGrupu(par.first).filter { kviz -> kviz.nazivPredmeta == par.second })
                return@withContext upisani
            }
        }

        suspend fun getDone(): List<Kviz> {
            return getUpisaniDB().filter { kviz ->  getColor(kviz) == R.drawable.plava }
        }

        suspend fun getFuture(): List<Kviz> {
            return getUpisaniDB().filter { kviz ->  getColor(kviz) == R.drawable.zuta }
        }

        suspend fun getNotTaken(): List<Kviz> {
            return getUpisaniDB().filter { kviz ->  getColor(kviz) == R.drawable.crvena }
        }

        private suspend fun getKvizoviZaGrupu(idGrupe: Int) : List<Kviz> {
            return withContext(Dispatchers.IO) {
                try {
                    val kvizovi = arrayListOf<Kviz>()
                    val url = ApiConfig.baseURL+"/grupa/$idGrupe/kvizovi"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val json = JSONArray(result)

                        for (i in 0 until json.length()) {
                            val jsonKviz = json.getJSONObject(i)
                            makeKvisko(jsonKviz)?.let { kvizovi.add(it) }
                        }
                    }
                    return@withContext kvizovi
                }catch (e: MalformedURLException) {
                    return@withContext emptyList<Kviz>()
                } catch (e: IOException) {
                    return@withContext emptyList<Kviz>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Kviz>()
                }
            }
        }

        fun spasiKviz(ktid: Int, zavrsen: Boolean) {
            stanja[ktid] = zavrsen
        }

        fun provjeriStanje(ktid: Int): Boolean {
            return stanja[ktid] ?: false
        }

        suspend fun zavrsiKviz(kvizId: Int) {
            val db = AppDatabase.getInstance(context)
            db.kvizDao().zavrsiKviz(kvizId)
        }
    }
}