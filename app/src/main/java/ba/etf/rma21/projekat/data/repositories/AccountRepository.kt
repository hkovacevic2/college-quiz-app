package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONException
import java.io.IOException
import java.net.MalformedURLException
import java.text.SimpleDateFormat
import java.util.*

class AccountRepository {
    companion object {
        var acHash : String = "43e71e9b-ad4d-4492-8820-d44a2a986119"
        private lateinit var context : Context
        fun setContext(_context : Context){
            context=_context
        }

        suspend fun postaviHash(hash: String) : Boolean {
            return withContext(Dispatchers.IO) {
                try {
                    pripremiRepos()
                    val db = AppDatabase.getInstance(context)
                    val trenutni = db.accountDao().getUser()
                    this@Companion.acHash = hash
                    if (trenutni != null) {
                        if (trenutni.acHash != hash) {
                            DBRepository.clearDB()
                            db.accountDao().clearUser()
                            val datum = Calendar.getInstance().time
                            db.accountDao().insert(Account(1, hash, SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(datum)))
                        }
                    } else {
                        val datum = Calendar.getInstance().time
                        db.accountDao().insert(Account(1, hash, SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(datum)))
                    }
                    if (hash.length < 36) return@withContext false
                    return@withContext true
                }catch (e: MalformedURLException) {
                    return@withContext false
                } catch (e: IOException) {
                    return@withContext false
                } catch (e: JSONException) {
                    return@withContext false
                }
            }
        }
        fun getHash() : String = acHash

        suspend fun getUser() : Account? {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                return@withContext db.accountDao().getUser()
            }
        }

        private fun pripremiRepos() {
            DBRepository.setContext(context)
            KvizRepository.setContext(context)
            OdgovorRepository.setContext(context)
            PitanjeKvizRepository.setContext(context)
            PredmetIGrupaRepository.setContext(context)
            TakeKvizRepository.setContext(context)
        }
    }

}