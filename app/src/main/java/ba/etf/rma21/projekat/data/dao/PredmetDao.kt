package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Predmet

@Dao
interface PredmetDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(predmeti: List<Predmet>)

    @Query("DELETE from predmet")
    suspend fun deleteAll()

    @Query("SELECT * FROM predmet")
    suspend fun getAll() : List<Predmet>
}