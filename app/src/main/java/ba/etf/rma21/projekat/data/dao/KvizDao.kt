package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface KvizDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(kvizovi: List<Kviz>)

    @Query("DELETE from kviz")
    suspend fun deleteAll()

    @Query("SELECT * from kviz")
    suspend fun getMyAll(): List<Kviz>

    @Query("UPDATE kviz SET predan=:zavrsen WHERE id=:kid")
    suspend fun zavrsiKviz(kid: Int, zavrsen: Boolean = true)

    @Query("UPDATE kviz SET osvojeniBodovi=:bodici WHERE id=:kid")
    suspend fun updateBodove(kid: Int, bodici: Int)
}