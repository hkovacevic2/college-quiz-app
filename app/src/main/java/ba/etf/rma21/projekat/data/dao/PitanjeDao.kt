package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface PitanjeDao {
    @Query("DELETE from pitanje")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pitanja: List<Pitanje>)

    @Query("SELECT * FROM pitanje WHERE KvizId=:kid")
    suspend fun getPitanjaZaKviz(kid : Int) : List<Pitanje>
}