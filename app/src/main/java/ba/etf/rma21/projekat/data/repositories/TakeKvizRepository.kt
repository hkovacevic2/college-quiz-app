package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class TakeKvizRepository {
    companion object {
        private lateinit var context : Context
        fun setContext(_context : Context){
            context=_context
        }

        suspend fun zapocniKviz(idKviza: Int) : KvizTaken? {
            return withContext(Dispatchers.IO) {
                try {
                    val pokusaji = getIdPocetih(getPokusajiJSON())
                    for (id in pokusaji) {
                        if (id == idKviza) return@withContext null
                    }
                    val url = ApiConfig.baseURL + "/student/${AccountRepository.acHash}/kviz/$idKviza"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        requestMethod = "POST"
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val jsonTaken = JSONObject(result)
                        if (jsonTaken.has("message")) return@withContext null
                        val id = jsonTaken.getInt("id")
                        val mapa = sklepajListu(idKviza,id)
                        val email = jsonTaken.getString("student")
                        val bodovi = jsonTaken.getInt("osvojeniBodovi")
                        val datumRada = SimpleDateFormat("yyyy-MM-dd").parse(jsonTaken.getString("datumRada"))
                        return@withContext KvizTaken(id, email, bodovi, datumRada, idKviza, mapa)
                    }
                } catch (e: MalformedURLException) {
                    return@withContext null
                } catch (e: IOException) {
                    return@withContext null
                } catch (e: JSONException) {
                    return@withContext null
                }
            }
        }

        suspend fun getPokusajiJSON() : JSONArray {
            val url = ApiConfig.baseURL + "/student/${AccountRepository.acHash}/kviztaken"
            val URL = URL(url)
            val konekcija = URL.openConnection() as HttpURLConnection
            val result = konekcija.inputStream.bufferedReader().use { it.readText() }
            return JSONArray(result)
        }

        fun getIdPocetih(pokusaji : JSONArray) : List<Int> {
            val ids = arrayListOf<Int>()
            for (i in 0 until pokusaji.length()) {
                val pokusaj = pokusaji.getJSONObject(i)
                ids.add(pokusaj.getInt("KvizId"))
            }
            return ids
        }

        private suspend fun sklepajListu(kid: Int, ktid: Int) : List<Pair<Pitanje,Odgovor?>> {
            return withContext(Dispatchers.IO) {
                PitanjeKvizRepository.setContext(context)
                val pitanja = PitanjeKvizRepository.getPitanjaDB(kid)
                OdgovorRepository.setContext(context)
                val odgovori = OdgovorRepository.getOdgovoriDB(kid)
                val lista  = arrayListOf<Pair<Pitanje,Odgovor?>>()
                for (pitanje in pitanja) {
                    if (odgovori == null) lista.add(Pair(pitanje,null))
                    else {
                        for (odgovor in odgovori) {
                            if (pitanje.id == odgovor.id) lista.add(Pair(pitanje,odgovor))
                        }
                        if (!lista.map { pair -> pair.first }.contains(pitanje)) lista.add(Pair(pitanje,null))
                    }
                }
                return@withContext lista
            }
        }

        suspend fun getPocetiKvizovi() : List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                try{
                    val pokusaji = arrayListOf<KvizTaken>()
                    val jsonPokusaji = getPokusajiJSON()
                    for (i in 0 until jsonPokusaji.length()) {
                        val pokusaj = jsonPokusaji.getJSONObject(i)
                        val kid = pokusaj.getInt("KvizId")
                        val id = pokusaj.getInt("id")
                        val mapa = sklepajListu(kid, id)
                        val email = pokusaj.getString("student")
                        val bodovi = KvizRepository.getUpisaniDB().first{ it.id == kid }.osvojeniBodovi
                        val datumRada = SimpleDateFormat("yyyy-MM-dd").parse(pokusaj.getString("datumRada"))
                        pokusaji.add(KvizTaken(id, email, bodovi!!, datumRada!!, kid, mapa))
                    }
                    return@withContext if(pokusaji.isEmpty()) null else pokusaji
                }catch (e: MalformedURLException) {
                    return@withContext null
                } catch (e: IOException) {
                    return@withContext null
                } catch (e: JSONException) {
                    return@withContext null
                }
            }
        }

        suspend fun dajPokusaj(idKviza: Int) : KvizTaken? {
            return withContext(Dispatchers.IO) {
                try {
                    val pokusaji = getPokusajiJSON()
                    for (i in 0 until pokusaji.length()) {
                        val pokusaj = pokusaji.getJSONObject(i)
                        if (pokusaj.getInt("KvizId") == idKviza) {
                            val id = pokusaj.getInt("id")
                            val mapa = sklepajListu(idKviza, id)
                            val email = pokusaj.getString("student")
                            val bodovi = KvizRepository.getUpisaniDB().first{ it.id == idKviza }.osvojeniBodovi
                            val datumRada = SimpleDateFormat("yyyy-MM-dd").parse(pokusaj.getString("datumRada"))
                            return@withContext KvizTaken(id, email, bodovi!!, datumRada!!, idKviza, mapa)
                        }
                    }
                    return@withContext null
                }catch (e: MalformedURLException) {
                    return@withContext null
                } catch (e: IOException) {
                    return@withContext null
                } catch (e: JSONException) {
                    return@withContext null
                }
            }
        }

        suspend fun getPokusaj(ktid: Int) : KvizTaken? {
            return withContext(Dispatchers.IO) {
                try {
                    val pokusaji = getPokusajiJSON()
                    for (i in 0 until pokusaji.length()) {
                        val pokusaj = pokusaji.getJSONObject(i)
                        if (pokusaj.getInt("id") == ktid) {
                            val id = ktid
                            val idKviza = pokusaj.getInt("KvizId")
                            val mapa = sklepajListu(idKviza, id)
                            val email = pokusaj.getString("student")
                            val bodovi = KvizRepository.getUpisaniDB().first{ it.id == idKviza }.osvojeniBodovi
                            val datumRada = SimpleDateFormat("yyyy-MM-dd").parse(pokusaj.getString("datumRada"))
                            return@withContext KvizTaken(id, email, bodovi!!, datumRada!!, idKviza, mapa)
                        }
                    }
                    return@withContext null
                }catch (e: MalformedURLException) {
                    return@withContext null
                } catch (e: IOException) {
                    return@withContext null
                } catch (e: JSONException) {
                    return@withContext null
                }
            }
        }
    }
}