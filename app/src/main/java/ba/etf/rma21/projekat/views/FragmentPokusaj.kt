package ba.etf.rma21.projekat.views

import android.content.Context
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.view.children
import androidx.core.view.forEachIndexed
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.controllers.MainActivity
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.viewmodels.KvizViewModel
import com.google.android.material.navigation.NavigationView

private const val GREEN = -12723068
private const val RED = -2404547

class FragmentPokusaj(
    private var pitanja: List<Pitanje>
) : Fragment() {
    private lateinit var manager : FragmentManager
    private lateinit var navMenu : NavigationView
    private lateinit var container: FrameLayout
    private lateinit var vm: KvizViewModel
    private var nazivKviza : String? = null
    private var zavrsen : Boolean = false
    private var pokusaj : KvizTaken? = null

    private val onNavigationItemSelectedListener = NavigationView.OnNavigationItemSelectedListener { item ->
        var position = -1
        navMenu.menu.forEachIndexed { index, child ->
            if (child.isChecked) {
                child.isChecked = false
            }
            if (child == item) {
                child.isChecked = true
                position = index
            }
        }
        manager.popBackStack()
        val transaction = manager.beginTransaction()
        if (position < pitanja.size) {
            val fragment = FragmentPitanje(pitanja[position]).apply {
                pokusaj?.let {
                    val par = it.pitanja[position]
                    arguments = Bundle().apply {
                        par.second?.let { it -> putInt("ANSWER", it.odgovoreno) }
                        putBoolean("DONE", zavrsen)
                    }
                }
            }
            transaction.replace(R.id.framePitanje, fragment, position.toString())
            transaction.addToBackStack("PITANJE")
        } else {
            pokusaj = vm.pokusaj.value
            val fragment = FragmentPoruka().apply {
                arguments = Bundle().apply {
                    putString("KVIZ", nazivKviza)
                    if (zavrsen) putInt("PROCENAT", this@FragmentPokusaj.arguments!!.getInt("BODICI"))
                    else putInt("PROCENAT", pokusaj!!.osvojeniBodovi)
                }
            }
            transaction.add(R.id.framePitanje, fragment, position.toString())
            transaction.addToBackStack("REZULTAT")
        }
        transaction.commit()
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            nazivKviza = requireArguments().getString("KVIZ")
            zavrsen = requireArguments().getBoolean("PREDAN")
        }
        val pokusajObserver = Observer<KvizTaken?> { kt ->
            if (kt != null) {
                pokusaj = kt
                if(!zavrsen) (context as MainActivity).changeMenu(R.id.predajKviz)
                populateNavBar()
                onNavigationItemSelectedListener.onNavigationItemSelected(navMenu.menu[0])
            }
        }
        vm.pokusaj.observe(this,pokusajObserver)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        vm = ViewModelProvider(requireActivity()).get(KvizViewModel::class.java)
    }

    override fun onDetach() {
        super.onDetach()
        vm.pokusaj.removeObservers(this)
        vm.pokusaj.postValue(null)
        vm.pitanja.postValue(null)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_pokusaj, container, false)
        manager = childFragmentManager
        navMenu = view.findViewById(R.id.navigacijaPitanja)
        this.container = view.findViewById(R.id.framePitanje)
        navMenu.setNavigationItemSelectedListener(onNavigationItemSelectedListener)
        return view
    }

    private fun populateNavBar() {
        if (pitanja.isNotEmpty()) {
            var i = 1
            while (i++ <= pitanja.size) {
                navMenu.menu.add(R.id.pitanje, i-2, i-2, (i-1).toString()).apply {
                    pokusaj?.let { it ->
                        val par = it.pitanja[i-2]
                        par.second?.let { it1 ->
                            val colored = SpannableString(this.title)
                            val color = if (par.first.tacan == it1.odgovoreno) GREEN else RED
                            colored.setSpan(ForegroundColorSpan(color),0, colored.length, 0)
                            this.title = colored
                        }
                    }
                }
            }
            navMenu.menu.children.first().isChecked = true
            if(zavrsen) navMenu.menu.add(R.id.rezultat, pitanja.size, pitanja.size, "REZULTAT")
        }
    }

    fun obojiNavBar(position: Int, color: Int) {
        val item = navMenu.menu[position]
        val colored = SpannableString(item.title)
        colored.setSpan(ForegroundColorSpan(color),0, colored.length, 0)
        item.title = colored
    }

    fun ubaciRezultat() {
        navMenu.menu.add(R.id.rezultat, pitanja.size, pitanja.size, "REZULTAT")
        onNavigationItemSelectedListener.onNavigationItemSelected(navMenu.menu[pitanja.size])
    }

    fun odgovoriNaPitanje(pid: Int, answer: Int) {
        pokusaj?.let {
            vm.postaviOdgovorKviz(it.id, pid, answer)
        }
    }

    fun predajOdgovore() {
        pokusaj?.let {
            vm.predajOdgovore(it.KvizId)
            vm.updateBodove(it.KvizId, it.osvojeniBodovi)
        }
    }

    companion object {
        fun newInstance(pitanja: List<Pitanje>) = FragmentPokusaj(pitanja)
    }
}