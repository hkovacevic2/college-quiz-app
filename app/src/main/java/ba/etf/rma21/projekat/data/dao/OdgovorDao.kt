package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface OdgovorDao {
    @Query("DELETE from odgovor")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(odgovor: Odgovor)

    @Query("SELECT * FROM odgovor WHERE id=:pid AND KvizId=:kid")
    suspend fun getOdgovor(kid: Int, pid: Int) : Odgovor?

    @Query("SELECT * FROM odgovor WHERE KvizId=:kid")
    suspend fun getOdgovoriZaKviz(kid: Int) : List<Odgovor>
}