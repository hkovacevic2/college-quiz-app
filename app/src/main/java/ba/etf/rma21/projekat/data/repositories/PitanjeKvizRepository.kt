package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL


class PitanjeKvizRepository {
    companion object {
        private lateinit var context : Context
        fun setContext(_context : Context){
            context=_context
        }

        suspend fun getPitanjaDB(idKviza: Int) : List<Pitanje> {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                return@withContext db.pitanjeDao().getPitanjaZaKviz(idKviza)
            }
        }

        suspend fun getPitanja(idKviza: Int) : List<Pitanje> {
            return withContext(Dispatchers.IO) {
                try {
                    val pitanja = arrayListOf<Pitanje>()
                    val url = ApiConfig.baseURL+"/kviz/$idKviza/pitanja"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = this.inputStream.bufferedReader().use { it.readText() }
                        val json = JSONArray(result)
                        for (i in 0 until json.length()) {
                            val pitanjeJSON = json.getJSONObject(i)
                            val opcije = pitanjeJSON.getJSONArray("opcije").toString()
                                .filter { c -> c!='[' && c!= ']' && c!='"' }
                            val id = pitanjeJSON.getInt("id")
                            val naziv = pitanjeJSON.getString("naziv")
                            val tekst = pitanjeJSON.getString("tekstPitanja")
                            val tacan = pitanjeJSON.getInt("tacan")
                            pitanja.add(Pitanje(id,naziv, tekst, opcije, tacan, idKviza))
                        }
                    }
                    return@withContext pitanja
                }catch (e: MalformedURLException) {
                    return@withContext emptyList<Pitanje>()
                } catch (e: IOException) {
                    return@withContext emptyList<Pitanje>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Pitanje>()
                }
            }
        }
    }
}