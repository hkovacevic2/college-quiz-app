package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PredmetIGrupaRepository {
    companion object {
        private lateinit var context : Context
        fun setContext(_context : Context){
            context=_context
        }

        suspend fun getPredmeti() : List<Predmet> {
            return withContext(Dispatchers.IO) {
                val predmeti = arrayListOf<Predmet>()
                try {
                    val url = ApiConfig.baseURL+"/predmet"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val json = JSONArray(result)
                        for (i in 0 until json.length()) {
                            val predmetJSON = json.getJSONObject(i)
                            predmeti.add(Predmet(predmetJSON.getInt("id"), predmetJSON.getString("naziv"), predmetJSON.getInt("godina")))
                        }
                    }
                    return@withContext predmeti
                }catch (e: MalformedURLException) {
                    return@withContext emptyList<Predmet>()
                } catch (e: IOException) {
                    return@withContext emptyList<Predmet>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Predmet>()
                }
            }
        }

        suspend fun getGrupe() : List<Grupa> {
            return withContext(Dispatchers.IO) {
                val grupe = arrayListOf<Grupa>()
                try {
                    val urlGrupe = ApiConfig.baseURL+"/grupa"
                    val urlPredmeti = ApiConfig.baseURL+"/predmet"
                    var URL = URL(urlGrupe)
                    val konekcija1 = URL.openConnection() as HttpURLConnection
                    konekcija1.run {
                        val resultGrupe = inputStream.bufferedReader().use { it.readText() }
                        URL = URL(urlPredmeti)
                        val konekcija2 = URL.openConnection() as HttpURLConnection
                        konekcija2.run {
                            val resultPredmeti = inputStream.bufferedReader().use { it.readText() }
                            val jsonGrupe = JSONArray(resultGrupe)
                            val jsonPredmeti = JSONArray(resultPredmeti)
                            for (i in 0 until jsonGrupe.length()) {
                                val grupaJSON = jsonGrupe.getJSONObject(i)
                                for (j in 0 until jsonPredmeti.length()) {
                                    val predmetJSON = jsonPredmeti.getJSONObject(j)
                                    if (grupaJSON.getInt("PredmetId") == predmetJSON.getInt("id"))
                                        grupe.add(Grupa(grupaJSON.getInt("id"),grupaJSON.getString("naziv"), predmetJSON.getString("naziv")))
                                }
                            }
                        }
                    }
                    return@withContext grupe
                }catch (e: MalformedURLException) {
                    return@withContext emptyList<Grupa>()
                } catch (e: IOException) {
                    return@withContext emptyList<Grupa>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Grupa>()
                }
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta:Int) : List<Grupa> {
            return withContext(Dispatchers.IO) {
                val grupe = arrayListOf<Grupa>()
                try {
                    var url = ApiConfig.baseURL+"/predmet/$idPredmeta/grupa"
                    var URL = URL(url)
                    val konekcija1 = URL.openConnection() as HttpURLConnection
                    return@withContext konekcija1.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val jsonGrupe = JSONArray(result)
                        url = ApiConfig.baseURL+"/predmet/$idPredmeta"
                        URL = URL(url)
                        val konekcija2 = URL.openConnection() as HttpURLConnection
                        val predmet = konekcija2.run {
                            val rez = inputStream.bufferedReader().use { it.readText() }
                            val jsonPredmet = JSONObject(rez)
                            if (jsonPredmet.has("message")) return@withContext grupe
                            return@run jsonPredmet.getString("naziv")
                        }
                        for (i in 0 until jsonGrupe.length()) {
                            val jsonGrupa = jsonGrupe.getJSONObject(i)
                            grupe.add(Grupa(jsonGrupa.getInt("id"), jsonGrupa.getString("naziv"), predmet))
                        }
                        return@run grupe
                    }
                } catch (e: MalformedURLException) {
                    return@withContext emptyList<Grupa>()
                } catch (e: IOException) {
                    return@withContext emptyList<Grupa>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Grupa>()
                }
            }
        }

        suspend fun getGrupeZaKviz(idKviza: Int) : List<Grupa> {
            return withContext(Dispatchers.IO) {
                try {
                    val grupe = arrayListOf<Grupa>()
                    val predmeti = getPredmeti()
                    val url = ApiConfig.baseURL+"/kviz/$idKviza/grupa"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val jsonGrupe = JSONArray(result)
                        for (i in 0 until jsonGrupe.length()) {
                            val grupaJSON = jsonGrupe.getJSONObject(i)
                            for (predmet in predmeti) {
                                if (grupaJSON.getInt("PredmetId") == predmet.id)
                                    grupe.add(Grupa(grupaJSON.getInt("id"),grupaJSON.getString("naziv"), predmet.naziv))
                            }
                        }
                    }
                    return@withContext grupe
                }catch (e: MalformedURLException) {
                    return@withContext emptyList<Grupa>()
                } catch (e: IOException) {
                    return@withContext emptyList<Grupa>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Grupa>()
                }
            }
        }

        suspend fun upisiUGrupu(idGrupa: Int) : Boolean {
            return withContext(Dispatchers.IO) {
                try {
                    val url = ApiConfig.baseURL+"/grupa/$idGrupa/student/${AccountRepository.acHash}"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        requestMethod = "POST"
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val msg = JSONObject(result).getString("message")
                        if (msg.contains("je dodan u grupu"))
                            return@withContext true
                        return@withContext false
                    }
                } catch (e: MalformedURLException) {
                    return@withContext false
                } catch (e: IOException) {
                    return@withContext false
                } catch (e: JSONException) {
                    return@withContext false
                }
            }
        }

        suspend fun getUpisanePredmete() : List<Predmet> {
            return withContext(Dispatchers.IO) {
                val predmeti = getPredmeti() as ArrayList<Predmet>
                try {
                    val url = ApiConfig.baseURL+"/student/${AccountRepository.acHash}/grupa"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val jsonGrupe = JSONArray(result)
                        return@withContext predmeti.filter {
                            for (i in 0 until jsonGrupe.length())
                                if (it.id == jsonGrupe.getJSONObject(i).getInt("PredmetId"))
                                    return@filter true
                            return@filter false
                        }
                    }
                } catch (e: MalformedURLException) {
                    return@withContext emptyList<Predmet>()
                } catch (e: IOException) {
                    return@withContext emptyList<Predmet>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Predmet>()
                }
            }
        }

        suspend fun getUpisaneGrupe() : List<Grupa> {
            return withContext(Dispatchers.IO) {
                val grupe = arrayListOf<Grupa>()
                try {
                    var url = ApiConfig.baseURL+"/student/${AccountRepository.acHash}/grupa"
                    var URL = URL(url)
                    val konekcija1 = URL.openConnection() as HttpURLConnection
                    return@withContext konekcija1.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val jsonGrupe = JSONArray(result)
                        url = ApiConfig.baseURL+"/predmet"
                        URL = URL(url)
                        val konekcija2 = URL.openConnection() as HttpURLConnection
                        konekcija2.run {
                            val rez = inputStream.bufferedReader().use { it.readText() }
                            val predmeti = getPredmeti()
                            for (i in 0 until jsonGrupe.length()) {
                                val grupa = jsonGrupe.getJSONObject(i)
                                predmeti.forEach { predmet ->
                                    if (predmet.id == grupa.getInt("PredmetId"))
                                        grupe.add(Grupa(grupa.getInt("id"), grupa.getString("naziv"), predmet.naziv))
                                }
                            }
                        }
                        return@run grupe
                    }
                } catch (e: MalformedURLException) {
                    return@withContext emptyList<Grupa>()
                } catch (e: IOException) {
                    return@withContext emptyList<Grupa>()
                } catch (e: JSONException) {
                    return@withContext emptyList<Grupa>()
                }
            }
        }
    }
}