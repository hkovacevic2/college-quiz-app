package ba.etf.rma21.projekat.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.core.view.children
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje

private const val GREEN = -12723068
private const val RED = -2404547

@SuppressLint("ResourceType")
class FragmentPitanje(private val pitanje: Pitanje) : Fragment() {
    private lateinit var tekstPitanja : TextView
    private lateinit var listaOdgovora : ListView
    private lateinit var callback: odgovorenoPitanje
    private var odgovoreno = -1

    interface odgovorenoPitanje {
        fun obojiNavBar(position: Int, color: Int)
    }

    private val onItemClickListener = AdapterView.OnItemClickListener { parent, view, _, _ ->
        view as TextView
        var answer = -1
        val green = Drawable.createFromXml(resources, resources.getXml(R.drawable.green))
        val red = Drawable.createFromXml(resources, resources.getXml(R.drawable.red))
        view.background = red
        odgovoreno = 0
        parent.children.forEachIndexed { index, child ->
            child as TextView
            if (view == child) answer = index
            if (index == pitanje.tacan) {
                child.background = green
                if (view == child) odgovoreno = 1
            }
        }
        (parentFragment as FragmentPokusaj).odgovoriNaPitanje(pitanje.id, answer)
        if (odgovoreno == 1) callback.obojiNavBar(this.tag!!.toInt(), GREEN)
        else callback.obojiNavBar(this.tag!!.toInt(), RED)
        parent.isEnabled = false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as odgovorenoPitanje
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_pitanje, container, false)
        tekstPitanja = view.findViewById(R.id.tekstPitanja)
        tekstPitanja.text = pitanje.tekstPitanja
        listaOdgovora = view.findViewById(R.id.odgovoriLista)
        listaOdgovora.onItemClickListener = onItemClickListener
        val adapter = object : ArrayAdapter<String> (view.context, android.R.layout.simple_list_item_1, pitanje.opcije.split(',')) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val vju = super.getView(position, convertView, parent)
                if (arguments?.size() != 1) {
                    vju as TextView
                    val green = Drawable.createFromXml(resources, resources.getXml(R.drawable.green))
                    val red = Drawable.createFromXml(resources, resources.getXml(R.drawable.red))
                    val data = arguments!!.getInt("ANSWER")
                    if (pitanje.tacan != data) {
                        when(position) {
                            pitanje.tacan -> vju.background = green
                            data -> vju.background = red
                        }
                    } else if (position == pitanje.tacan) vju.background = green
                    odgovoreno = data
                    if (odgovoreno != -1) listaOdgovora.isEnabled = false
                }
                if (arguments?.getBoolean("DONE")!!) listaOdgovora.isEnabled = false
                return vju
            }
        }
        listaOdgovora.adapter = adapter
        return view
    }

    companion object {
        fun newInstance(pitanje: Pitanje) = FragmentPitanje(pitanje)
    }
}