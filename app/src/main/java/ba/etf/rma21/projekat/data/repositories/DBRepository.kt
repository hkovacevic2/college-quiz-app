package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class DBRepository {
    companion object {
        private lateinit var context : Context
        fun setContext(_context : Context){
            context=_context
        }

        suspend fun updateNow() : Boolean {
            return withContext(Dispatchers.IO) {
                try {
                    val db = AppDatabase.getInstance(context)
                    val acc = db.accountDao().getUser()
                    val url = "https://rma21-etf.herokuapp.com/account/${acc!!.acHash}/lastUpdate?date=${acc.lastUpdate}"
                    val URL = URL(url)
                    val konekcija = URL.openConnection() as HttpURLConnection
                    konekcija.run {
                        val result = inputStream.bufferedReader().use { it.readText() }
                        val json = JSONObject(result)
                        return@withContext json.getBoolean("changed")
                    }
                } catch (e: MalformedURLException) {
                    return@withContext false
                } catch (e: IOException) {
                    return@withContext false
                } catch (e: JSONException) {
                    return@withContext false
                }
            }
        }

        suspend fun checkUpdate() {
            if (updateNow()) {
                val db = AppDatabase.getInstance(context)
                clearDB()
                updateDB()
                db.accountDao().updateHash(SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().time))
            }
        }

        suspend fun clearDB() {
            val db = AppDatabase.getInstance(context)
            db.kvizDao().deleteAll()
            db.predmetDao().deleteAll()
            db.grupaDao().deleteAll()
            db.pitanjeDao().deleteAll()
            db.odgovorDao().deleteAll()
        }

        suspend fun updateDB() {
            val db = AppDatabase.getInstance(context)
            val kvizovi = KvizRepository.getUpisani()
            db.kvizDao().insertAll(kvizovi)
            db.predmetDao().insertAll(PredmetIGrupaRepository.getUpisanePredmete())
            db.grupaDao().insertAll(PredmetIGrupaRepository.getUpisaneGrupe())
            kvizovi.forEach { db.pitanjeDao().insertAll(PitanjeKvizRepository.getPitanja(it.id)) }
        }
    }
}