package ba.etf.rma21.projekat.controllers

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.interfaces.PopupListener
import ba.etf.rma21.projekat.interfaces.SuccessListener
import ba.etf.rma21.projekat.viewmodels.KvizViewModel
import ba.etf.rma21.projekat.views.*
import com.google.android.material.bottomnavigation.BottomNavigationView

private const val GREEN = -12723068
private const val RED = -2404547

class MainActivity : AppCompatActivity(), SuccessListener, PopupListener, FragmentPitanje.odgovorenoPitanje {

    private val manager = supportFragmentManager
    private lateinit var transaction: FragmentTransaction
    private lateinit var bottomNav: BottomNavigationView
    private lateinit var kvizovi: FragmentKvizovi
    private lateinit var predmeti: FragmentPredmeti
    private lateinit var popup: FragmentPokusaj
    private lateinit var poruka: FragmentPoruka
    private lateinit var vm: KvizViewModel

    private val onNavItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        transaction = manager.beginTransaction()
        when (item.itemId) {
            R.id.kvizovi -> {
                manager.popBackStack("BASE",0)
                repopulateFragment(kvizovi)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                transaction.hide(kvizovi)
                if (this::popup.isInitialized) {
                    transaction.hide(popup)
                }
                if (this::poruka.isInitialized) {
                    transaction.hide(poruka)
                }
                transaction.show(predmeti)
                transaction.addToBackStack(null)
                transaction.commit()
                repopulateFragment(predmeti)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predajKviz -> {
                popup.predajOdgovore()
                popup.ubaciRezultat()
                changeMenu(R.id.kvizovi)
                return@OnNavigationItemSelectedListener true
            }
            R.id.zaustaviKviz -> {
                manager.popBackStack("BASE",0)
                repopulateFragment(kvizovi)
                changeMenu(R.id.kvizovi)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun initFragments() {
        kvizovi = FragmentKvizovi()
        predmeti = FragmentPredmeti()
        transaction = manager.beginTransaction()
        transaction.add(R.id.fragment_container, predmeti).hide(predmeti)
        transaction.add(R.id.fragment_container, kvizovi)
        transaction.addToBackStack("BASE")
        transaction.commit()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("SELECTED",bottomNav.selectedItemId)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        for (fragment in manager.fragments) {
            if (fragment is FragmentPredmeti) {
                predmeti = fragment
            }
            if (fragment is FragmentKvizovi) {
                kvizovi = fragment
            }
            if (fragment is FragmentPokusaj) {
                popup = fragment
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        val payload: String? = intent?.getStringExtra("payload")
        if (payload != null) vm.postaviHash(payload)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        vm = ViewModelProvider(this).get(KvizViewModel::class.java)
        AccountRepository.setContext(this)
        val payload: String? = intent?.getStringExtra("payload")
        if (payload != null) vm.postaviHash(payload)
        else vm.postaviHash("43e71e9b-ad4d-4492-8820-d44a2a986119")
        bottomNav = findViewById(R.id.bottomNav)
        if(savedInstanceState == null) initFragments()
        else {
            when (savedInstanceState.getInt("SELECTED")) {
                R.id.predajKviz -> changeMenu(R.id.predajKviz)
                R.id.kvizovi -> changeMenu(R.id.kvizovi)
            }
        }
        bottomNav.setOnNavigationItemSelectedListener(onNavItemSelectedListener)
        bottomNav.setOnNavigationItemReselectedListener { item ->
            onNavItemSelectedListener.onNavigationItemSelected(item)
        }
        this.onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                when (bottomNav.selectedItemId) {
                    R.id.kvizovi -> finish()
                    R.id.predmeti -> {
                        onNavItemSelectedListener.onNavigationItemSelected(bottomNav.menu.findItem(R.id.kvizovi))
                    }
                    R.id.predajKviz -> {
                        onNavItemSelectedListener.onNavigationItemSelected(bottomNav.menu.findItem(R.id.zaustaviKviz))
                    }
                }
            }
        })
    }

    private fun repopulateFragment(fragment: Fragment) {
        transaction = manager.beginTransaction()
        transaction.detach(fragment)
        transaction.attach(fragment)
        transaction.commit()
    }

    override fun showMessage(fragment: Fragment) {
        transaction = manager.beginTransaction()
        poruka = fragment as FragmentPoruka
        transaction.hide(predmeti).add(R.id.fragment_container, poruka,"PORUKA")
        transaction.addToBackStack("Prikaz poruke")
        transaction.commit()
    }

    override fun startKvisko(kviz: Kviz, pitanja: List<Pitanje>) {
        transaction = manager.beginTransaction()
        popup = FragmentPokusaj(pitanja).apply {
            val bundle = Bundle()
            bundle.putString("KVIZ", kviz.naziv)
            bundle.putBoolean("PREDAN", kviz.predan)
            if(kviz.predan) bundle.putInt("BODICI", kviz.osvojeniBodovi!!)
            arguments = bundle
        }
        transaction.hide(kvizovi).add(R.id.fragment_container, popup, "POPUP")
        transaction.addToBackStack("Popup")
        transaction.commit()
    }


    fun changeMenu(fragment : Int) {
        when (fragment) {
            R.id.kvizovi -> {
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = false
                bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = false
                bottomNav.menu.findItem(R.id.kvizovi).isVisible = true
                bottomNav.menu.findItem(R.id.predmeti).isVisible = true
                bottomNav.menu.findItem(R.id.kvizovi).isChecked = true
            }
            R.id.predajKviz -> {
                bottomNav.menu.findItem(R.id.kvizovi).isVisible = false
                bottomNav.menu.findItem(R.id.predmeti).isVisible = false
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = true
                bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = true
                bottomNav.menu.findItem(R.id.predajKviz).isChecked = true
            }
        }
    }

//    override fun setObserver(vlasnik: FragmentPitanje, odgObserver: Observer<List<Odgovor?>>) {
//        vm.odgovori.observe(vlasnik, odgObserver)
//    }

    override fun obojiNavBar(position: Int, color: Int) {
        popup.obojiNavBar(position, color)
    }
}