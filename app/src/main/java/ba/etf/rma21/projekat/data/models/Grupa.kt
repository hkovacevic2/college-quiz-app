package ba.etf.rma21.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Grupa(@PrimaryKey @SerializedName("id") val id: Int,
                 @ColumnInfo(name = "naziv") val naziv: String,
                 @ColumnInfo(name = "nazivPredmeta") val nazivPredmeta: String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(naziv)
        parcel.writeString(nazivPredmeta)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Grupa

        if (id != other.id) return false
        if (naziv != other.naziv) return false
        if (nazivPredmeta != other.nazivPredmeta) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + naziv.hashCode()
        result = 31 * result + nazivPredmeta.hashCode()
        return result
    }

    override fun toString(): String {
        return naziv
    }

    companion object CREATOR : Parcelable.Creator<Grupa> {
        override fun createFromParcel(parcel: Parcel): Grupa {
            return Grupa(parcel)
        }

        override fun newArray(size: Int): Array<Grupa?> {
            return arrayOfNulls(size)
        }
    }
}