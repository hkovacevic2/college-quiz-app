package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account

@Dao
interface AccountDao {
    @Query("DELETE from account")
    suspend fun clearUser()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: Account)

    @Query("SELECT * from account WHERE id=1")
    suspend fun getUser() : Account?

    @Query("UPDATE account SET lastUpdate=:lastUpdate WHERE id=1")
    suspend fun updateHash(lastUpdate: String)
}