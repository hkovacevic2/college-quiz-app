package ba.etf.rma21.projekat.interfaces

import androidx.fragment.app.Fragment

interface SuccessListener {
    fun showMessage(fragment: Fragment)
}