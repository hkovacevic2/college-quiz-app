package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Grupa

@Dao
interface GrupaDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(grupe: List<Grupa>)
    @Query("DELETE from grupa")
    suspend fun deleteAll()
    @Query("SELECT * from grupa")
    suspend fun getAll() : List<Grupa>
}