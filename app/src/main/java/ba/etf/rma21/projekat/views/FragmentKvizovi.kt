package ba.etf.rma21.projekat.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.DBRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import ba.etf.rma21.projekat.interfaces.PopupListener
import ba.etf.rma21.projekat.viewmodels.KvizViewModel

class FragmentKvizovi : Fragment() {

    private lateinit var recikliran : RecyclerView
    private lateinit var vm : KvizViewModel
    private lateinit var adapter: KvizAdapter
    private lateinit var spinner: Spinner
    private lateinit var popupListener: PopupListener
    private lateinit var stisnut: Kviz

    override fun onAttach(context: Context) {
        super.onAttach(context)
        popupListener = context as PopupListener
        vm = ViewModelProvider(requireActivity()).get(KvizViewModel::class.java)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (this::spinner.isInitialized) {
            if (!hidden) update(spinner.selectedItemPosition)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val kvizoviObserver = Observer<List<Kviz>> { fresh -> adapter.updateCategory(fresh) }
        vm.kvizovi.observeForever(kvizoviObserver)
        val pitanjaObserver = Observer<List<Pitanje>?> { fresh ->
            fresh?.run { popupListener.startKvisko(stisnut, this) }
        }
        val userObserver = Observer<Account> { update(0) }
        vm.user.observeForever(userObserver)
        vm.pitanja.observeForever(pitanjaObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_kvizovi, container, false)
        spinner = view.findViewById(R.id.filterKvizova)
        val spinnerAdapter = activity?.let { ArrayAdapter.createFromResource(it, R.array.Izbor, android.R.layout.simple_spinner_dropdown_item) }
        spinnerAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = spinnerAdapter
        recikliran = view.findViewById(R.id.listaKvizova)
        adapter = activity?.let { KvizAdapter(it, emptyList()) {kviz ->
                vm.getPitanja(kviz)
                vm.zapocniKviz(kviz.id)
                stisnut = kviz
            }
        }!!
        val layoutManager = GridLayoutManager(activity, 2)
        recikliran.layoutManager = layoutManager
        recikliran.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                update(position)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        return view
    }

    private fun update(position: Int) {
        when(position) {
            0 -> vm.getUpisani()
            1 -> vm.getAll()
            2 -> vm.getDone()
            3 -> vm.getFuture()
            4 -> vm.getNotTaken()
        }
    }

    companion object {
        fun newInstance() = FragmentKvizovi()
    }
}