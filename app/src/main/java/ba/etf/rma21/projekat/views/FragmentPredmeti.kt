package ba.etf.rma21.projekat.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.DBRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.interfaces.SuccessListener
import ba.etf.rma21.projekat.viewmodels.UpisViewModel
import java.util.*

class FragmentPredmeti : Fragment() {
    private lateinit var vm : UpisViewModel
    private lateinit var godineAdapter : ArrayAdapter<CharSequence>
    private lateinit var predmetiAdapter : ArrayAdapter<String>
    private lateinit var grupeAdapter : ArrayAdapter<String>
    private lateinit var btnUpisi : Button
    private lateinit var successListener: SuccessListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        successListener = context as SuccessListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = UpisViewModel()
        val predmetiObserver = Observer<List<Predmet>> { fresh -> predmetiAdapter.run {
            clear()
            addAll(fresh.map { predmet -> predmet.naziv })
            notifyDataSetChanged()
        }}
        vm.predmeti.observeForever(predmetiObserver)
        val grupeObserver = Observer<List<Grupa>> { fresh -> grupeAdapter.run {
            clear()
            addAll(fresh.map { grupa -> grupa.naziv })
            notifyDataSetChanged()
            btnUpisi.isVisible = !grupeAdapter.isEmpty
        }}
        vm.grupe.observeForever(grupeObserver)
        vm.dajPredmete(1)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_predmeti, container, false)
        super.onCreate(savedInstanceState)

        btnUpisi = view.findViewById(R.id.dodajPredmetDugme)

        val spinnerGodine = view.findViewById<Spinner>(R.id.odabirGodina)
        godineAdapter = activity?.let { ArrayAdapter.createFromResource(it, R.array.Godine, android.R.layout.simple_spinner_item) }!!
        godineAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerGodine.adapter = godineAdapter

        val spinnerPredmeti = view.findViewById<Spinner>(R.id.odabirPredmet)
        predmetiAdapter = ArrayAdapter<String>(requireActivity(), android.R.layout.simple_spinner_item, arrayListOf<String>())
        predmetiAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerPredmeti.adapter = predmetiAdapter

        val spinnerGrupe = view.findViewById<Spinner>(R.id.odabirGrupa)
        grupeAdapter = ArrayAdapter<String>(requireActivity(), android.R.layout.simple_spinner_item, arrayListOf<String>())
        grupeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerGrupe.adapter = grupeAdapter

        spinnerGodine.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                predmetiAdapter.clear()
                vm.dajPredmete(position+1)
                spinnerPredmeti.onItemSelectedListener?.onItemSelected(parent, view, 0, id)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinnerPredmeti.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                grupeAdapter.clear()
                if(spinnerPredmeti.selectedItem == null) spinnerPredmeti.setSelection(0)
                vm.getGrupe(spinnerPredmeti.selectedItem?.toString())
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        btnUpisi.setOnClickListener { _ ->
            vm.grupe.value?.first { grupa -> grupa.naziv == spinnerGrupe.selectedItem.toString() }?.run {
                vm.upisiUGrupu(this.id)
                val msg = FragmentPoruka().apply {
                    arguments = Bundle().apply { putParcelable("GRUPA", this@run) }
                }
                successListener.showMessage(msg)
                (spinnerGodine.onItemSelectedListener as AdapterView.OnItemSelectedListener)
                    .onItemSelected(null, null, spinnerGodine.selectedItemPosition, 1L)
            }
        }
        return view
    }

    companion object {
        fun newInstance() = FragmentPredmeti()
    }
}