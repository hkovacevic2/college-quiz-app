package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Pitanje(@PrimaryKey val id: Int,
                   val naziv: String,
                   val tekstPitanja: String,
                   val opcije: String,
                   val tacan: Int,
                   val KvizId: Int)