package ba.etf.rma21.projekat.data.models

import java.util.*

data class KvizTaken(val id: Int,
                     val student: String,
                     var osvojeniBodovi: Int,
                     val datumRada: Date,
                     val KvizId: Int,
                     val pitanja: List<Pair<Pitanje,Odgovor?>>)