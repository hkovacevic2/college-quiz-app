package ba.etf.rma21.projekat.views

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import java.text.SimpleDateFormat
import java.util.*


class KvizAdapter(
        private var context: Context,
        private var listaKvizova: List<Kviz>,
        private var onClick: (kviz : Kviz) -> Unit
) : RecyclerView.Adapter<KvizAdapter.Holder>() {

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val stanjeKviska = itemView.findViewById<ImageView>(R.id.stanjeKviza)
        val datumKviska = itemView.findViewById<TextView>(R.id.datumTxt)
        val imeKviska = itemView.findViewById<TextView>(R.id.nazivKvizaTxt)
        val nazivPredmeta = itemView.findViewById<TextView>(R.id.nazivPredmetaTxt)
        val bodovi = itemView.findViewById<TextView>(R.id.bodoviTxt)
        val trajanje = itemView.findViewById<TextView>(R.id.trajanjeKvizaTxt)

        fun bindKvisko(kvisko: Kviz, context: Context) {
            val id = getKruzic(kvisko)
            stanjeKviska.setImageResource(id)
            datumKviska.text = getDesiredDate(kvisko)
            imeKviska.text = kvisko.naziv
            nazivPredmeta.text = kvisko.nazivPredmeta
            bodovi.text = if (id == R.drawable.plava) kvisko.osvojeniBodovi.toString() else ""
            trajanje.text = kvisko.trajanje.toString()
        }
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.kvisko_item, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindKvisko(listaKvizova[position], context)
        holder.itemView.setOnClickListener { onClick(listaKvizova[position]) }
    }

    override fun getItemCount() = listaKvizova.size



    fun getKruzic(kviz: Kviz) : Int {
        val atm = Calendar.getInstance().time
        if (atm.before(toDate(kviz.datumKraj))) {
            if (atm.before(toDate(kviz.datumPocetka)))
                return R.drawable.zuta
            return R.drawable.zelena
        } else {
            if (kviz.datumRada == null)
                return R.drawable.crvena
            return R.drawable.plava
        }
    }

    private fun toDate(string: String?): Date? {
        if(string.equals(null)) return null
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        return dateFormat.parse(string)
    }

    fun getDesiredDate(kviz: Kviz) : String {
        val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        val date = when(getKruzic(kviz)) {
            R.drawable.plava -> kviz.datumRada
            R.drawable.zuta -> kviz.datumPocetka
            else -> kviz.datumKraj
        }
        return formatter.format(toDate(date)!!)
    }

    fun updateCategory(lista: List<Kviz>?) {
        if (lista != null) {
            listaKvizova = lista
        }
        notifyDataSetChanged()
    }
}