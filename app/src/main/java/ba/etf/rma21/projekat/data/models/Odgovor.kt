package ba.etf.rma21.projekat.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Odgovor(@PrimaryKey val id: Int,
                   val odgovoreno: Int,
                   val KvizId: Int)