//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import ba.etf.rma21.projekat.viewmodels.KvizViewModel
//import junit.framework.Assert.assertEquals
//import org.junit.Test
//
//class ListeKvizovaUnitTest {
//    @Test
//    fun testDajMojeKvizove() {
//        val kvizListViewModel = KvizViewModel()
//        assertEquals(KvizRepository.getMyKvizes().size,kvizListViewModel.getMyKvizes().size)
//    }
//    @Test
//    fun testDajSveKvizove() {
//        val kvizListViewModel = KvizViewModel()
//        assertEquals(10,kvizListViewModel.getAll().size)
//    }
//    @Test
//    fun testDajUradjeneKvizove() {
//        val kvizListViewModel = KvizViewModel()
//        assertEquals(2,kvizListViewModel.getDone().size)
//    }
//    @Test
//    fun testDajBuduceKvizove() {
//        val kvizListViewModel = KvizViewModel()
//        assertEquals(2,kvizListViewModel.getFuture().size)
//    }
//    @Test
//    fun testDajProsleKvizove() {
//        val kvizListViewModel = KvizViewModel()
//        assertEquals(2,kvizListViewModel.getDone().size)
//    }
//    @Test
//    fun testDajMojeUradjeneKvizove() {
//        val kvizListViewModel = KvizViewModel()
//        assertEquals(1,kvizListViewModel.getDone().size)
//    }
//    @Test
//    fun testDajMojeBuduceKvizove() {
//        val kvizListViewModel = KvizViewModel()
//        assertEquals(0,kvizListViewModel.getFuture().size)
//    }
//    @Test
//    fun testDajMojeProsleKvizove() {
//        val kvizListViewModel = KvizViewModel()
//        assertEquals(1,kvizListViewModel.getNotTaken().size)
//    }
//}